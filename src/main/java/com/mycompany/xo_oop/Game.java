/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.xo_oop;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
class Game {

    private Player player1;
    private Player player2;
    private Table table;
    
    public Game() {
        this.player1 = new Player("O");
        this.player2 = new Player("X");
    }
    public void newGame() {
        this.table = new Table( player1, player2);
    }

    public void play() {
        printWelcome();
        newGame();
        while(true){
            printTable();
            printTurn();
            InputNum();
            if(table.checkWin()){
                printTable();
                printWin();
                showInfo();
                newGame();
                break;
            }
            if(table.checkDraw(table.getCountTurn())){
                printTable();
                printDraw();
                showInfo();
                newGame();
                break;
            }
            table.switchPlayer();
        }
    }

    private void printWelcome() {
        System.out.println("Welcome to OX Games");
        System.out.println("Table of numbers");
    }

    private void printTable() {
        String[][] t = table.getTable();
        for(int i=0;i<3;i++) {
            for(int j=0;j<3;j++){
                System.out.print(t[i][j]+" ");
            }
            System.out.println("");
        }

    }

    private void printTurn() {
        System.out.println("Turn "+table.getCurrentPlayer());
    }

    private void InputNum() {
        Scanner kb = new Scanner(System.in);
        while(true){
            System.out.print("Please Input Number to Place : ");
            int num = kb.nextInt();
            if(table.setRowCol(num)){
                break;
            }
        }
    }

    private void printWin() {
        System.out.println(table.getCurrentPlayer() +" Win !!!");
    }

    private void printDraw() {
        System.out.println("Draw!!!");
    }

    private void showInfo() {
        System.out.println(player1);
        System.out.println(player2);
    }
    
}
