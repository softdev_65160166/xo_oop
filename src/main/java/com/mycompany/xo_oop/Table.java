/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.xo_oop;

/**
 *
 * @author informatics
 */
public class Table {
    private Player player1;

    public int getCountTurn() {
        return countTurn;
    }
    private Player player2;
    private Player currentPlayer;
    private String[][] table={{"1","2","3"}, { "4", "5", "6" }, { "7", "8", "9" } };
    private int countTurn = 1;
    public Table(Player player1,Player player2){
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }

    public String getCurrentPlayer() {
        return currentPlayer.getSymbol();
    }

    public String[][] getTable() {
        return table;
    }
    
    public boolean setRowCol(int num){
        int col = (num-1)%3;
        int row = (num-1)/3;
        if (table[row][col].equals("X") || table[row][col].equals("O")) {
            System.out.println("Try Again!!!");
            return false;
        }
        table[row][col] = currentPlayer.getSymbol();
        return true;
        

    }
    public void switchPlayer(){
        countTurn++;
        if(currentPlayer == player1){
            currentPlayer = player2;
        }else {
            currentPlayer = player1;
        }
    }
    public boolean checkWin(){
        saveWin();
        return checkRow(table, currentPlayer)|| checkCol(table, currentPlayer)|| checkDiag(table, currentPlayer)||checkAntiDiag(table, currentPlayer);
    }
    private static boolean checkRow(String[][] table, Player currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (table[i][0].equals(currentPlayer.getSymbol()) && table[i][1].equals(currentPlayer.getSymbol()) && table[i][2].equals(currentPlayer.getSymbol())) {
                return true;
            }
        }
        return false;
    }

    private static boolean checkCol(String[][] table, Player currentPlayer) {
        for (int i = 0; i < 3; i++) {
            if (table[0][i].equals(currentPlayer.getSymbol()) && table[1][i].equals(currentPlayer.getSymbol()) && table[2][i].equals(currentPlayer.getSymbol())) {
                return true;
            }
        }
        return false;

    }
    
    private static boolean checkDiag(String[][] table, Player currentPlayer) {
        return (table[0][0].equals(currentPlayer.getSymbol()) && table[1][1].equals(currentPlayer.getSymbol()) && table[2][2].equals(currentPlayer.getSymbol()));
    }
    
    private static boolean checkAntiDiag(String[][] table, Player currentPlayer) {
        return (table[0][2].equals(currentPlayer.getSymbol())&& table[1][1].equals(currentPlayer.getSymbol())&& table[2][0].equals(currentPlayer.getSymbol()));
    }
    
    public boolean checkDraw(int countTurn) {
        saveDraw();
        return countTurn == 9;
    }
    
    public void saveWin() {
        if(player1 == currentPlayer) {
            player1.win();
            player2.lose();
        }else {
            player2.win();
            player1.lose();
        }
    }
    public void saveDraw() {
        player1.draw();
        player2.draw();
    }
    


}
    
    
        
    
    

