/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.xo_oop;

/**
 *
 * @author informatics
 */
public class Player {
    private String symbol;
    private int winCount;
    private int loseCount;
    private int drawCount;
    
    public Player(String symbol){
        this.symbol = symbol;
        this.winCount = 0;
        this.loseCount = 0;
        this.drawCount = 0;
    }

    public String getSymbol() {
        return symbol;
    }

    public int getWinCount() {
        return winCount;
    }

    public int getLoseCount() {
        return loseCount;
    }

    public int getDrawCount() {
        return drawCount;
    }
    public void win() {
        winCount++;
    }
     public void draw() {
        drawCount++;
    }
      public void lose() {
        loseCount++;
    }

    @Override
    public String toString() {
        return "Player{" + "player=" + symbol + ", winCount=" + winCount + ", loseCount=" + loseCount + ", drawCount=" + drawCount + '}';
    }
      
    
}
